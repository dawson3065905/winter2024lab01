//from https://gitlab.com/lucagallay/winter2024lab01/

import java.util.Scanner;  

public class Hangman {
    public static void main(String []args) {
        Scanner input = new Scanner(System.in);
		
        System.out.println("Enter a four-lettered word: ");
        String word = input.nextLine();
        
		//upper cases the word for compat
        runGame(word.toUpperCase());

        input.close(); 
    }

    public static int isLetterInWord(String word, char c) {
        for (int i = 0; i < word.length(); i++) {
			//searches through "word" to find c
			//returns i directly if it finds c 
			//i represents index
            if (word.charAt(i) == c) {
                return i;
            }
        }
		//returns -1 if it cannot find index
        return -1;
    }
	
    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }
	
	public static void printWork(String word, boolean[] boxOfSwitches) {
        System.out.print("Your result is ");
		//for building string
		String resultWord = "";
		
		for(int i = 0; i < boxOfSwitches.length; i++){
			//if switch is true then extract letter
			//then add it to the string we are building
			if(boxOfSwitches[i]){
				resultWord = resultWord + word.charAt(i);
			}
			//otherwise, put "_" as placeg
			else
				resultWord = resultWord + "_";
		}
		
		//prints out single built string
        System.out.println(resultWord);
    }
	
	public static void runGame(String word) {
        int triesLeft = 6;
		
		//Container for booleans, used for printWork
		boolean[] letterSwitchBox = new boolean[] {false, false, false, false};
        
  

        Scanner input = new Scanner(System.in);
        
		//not letterSwitchBox because it starts at false
		//uses || because of boolean math and because all of them needs to be true to take effect
		//additional parenthese around letterbox because we need triesleft to work
        while (triesLeft > 0 && (!letterSwitchBox[0] || !letterSwitchBox[1] || !letterSwitchBox[2] || !letterSwitchBox[3])) {
            System.out.println("Guess a letter: ");

            char c = toUpperCase(input.next().charAt(0));
			
			//see where the char is
			//-1 = no char
            int charPosition = isLetterInWord(word, c);

			
			if(charPosition != -1){
				letterSwitchBox[charPosition] = true;
			}
			//if it does not find the character
			//lose try and prints taunt
			else{
				triesLeft -= 1;
				System.out.println("Nope, sorry. You have " + triesLeft +" tries left. ");
			}
			
			//call printWork using letterSwitchBox
			//replaces old boolean system
			printWork(word, letterSwitchBox);
        }
		
		//if the player exits with no tries left
		//it means they exhausted all their tries
		//therefore they loose
		if (triesLeft != 0) {
            System.out.println("You won!");
        }
		else{
			System.out.println("You lost.");
		}

        input.close();
    }
 
}